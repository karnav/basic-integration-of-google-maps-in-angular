import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Marker } from '../shared/marker';
import { MarkerService } from '../services/marker.service';

@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.component.html',
  styleUrls: ['./googlemap.component.scss']
})
export class GooglemapComponent implements OnInit {

  marker: Marker;
  addMarkerForm: FormGroup;

  //values
  markerName: string;
  markerLat: string;
  markerLng: string;
  markerDraggable: string;

  //Zoom level
  zoom: number = 10;

  //Start position
  lat: number = 19.229392;
  lng: number = 72.856994;

  //Markers
  markers: Marker[];

  constructor(
    private markerService: MarkerService
  ) { 
    this.markers = this.markerService.getMarkers();
  }

  clickedMarker(marker: Marker, index: number) {
    console.log('Clicked Marker: ' + marker.name + ' at index ' + index);
  }

  mapClicked($event: any) {
    var newMarker = {
      name: 'Untitled',
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: false
    }

    this.markers.push(newMarker);
  }

  markerDragEnd(marker: any, $event: any) {
    console.log('dragEnd ', marker, $event );

    var updMarker = {
      name: marker.name,
      lat: parseFloat(marker.lat),
      lng: parseFloat(marker.lng),
      draggable: false
    }

    var newLat = $event.coords.lat;
    var newLng = $event.coords.lng;

    this.markerService.updateMarker(updMarker, newLat, newLng);
  }

  ngOnInit() {
  }

  addMarker() {
    console.log('Adding Marker');
    if (this.markerDraggable === 'yes') {
      var isDraggable = true;
    } else {
      var isDraggable = false;
    }

    var newMarker = {
      name: this.markerName,
      lat: parseFloat(this.markerLat),
      lng: parseFloat(this.markerLng),
      draggable: isDraggable
    }

    this.markers.push(newMarker);
    this.markerService.addMarker(newMarker);
    // this.marker = this.addMarkerForm.value;
    // console.log('New marker ', this.marker);
    // this.markers.push(this.marker);
  }

  removeMarker(marker) {
    console.log('Removing marker...');

    for(var i=0; i < this.markers.length; i++) {
      if(marker.lat === this.markers[i].lat && marker.lng === this.markers[i].lng) {
        this.markers.splice(i, 1);
      }
    }

    this.markerService.removeMarker(marker);
  }

}
