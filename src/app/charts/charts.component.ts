import { Component, OnInit } from '@angular/core';
// import { WeatherService } from '../services/weather.service';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {

  myChart = [];
  constructor() { }

  ngOnInit() {
    var ctx = 'myChart';
    Chart.defaults.global.defaultFontFamily = 'Lato';
    Chart.defaults.global.defaultFontSize = 18;
    Chart.defaults.global.defaultFontColor = '#777';

    let massPopChart = new Chart(ctx, {
      type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
      data:{
        labels:['Mumbai', 'Delhi', 'Bangalore', 'Ahmedabad', 'Chennai', 'Kolkata'],
        datasets:[{
          label:'Population',
          data:[
            16753235,
            12478447,
            8425970,
            5570585,
            4681087,
            4486679
          ],
          //backgroundColor:'green',
          backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(255, 99, 132, 0.6)',
            'rgba(255, 99, 132, 0.6)',
            'rgba(255, 99, 132, 0.6)',
            'rgba(255, 99, 132, 0.6)',
            'rgba(255, 99, 132, 0.6)',
            'rgba(255, 99, 132, 0.6)'
            // 'rgba(54, 162, 235, 0.6)',
            // 'rgba(255, 206, 86, 0.6)',
            // 'rgba(75, 192, 192, 0.6)',
            // 'rgba(153, 102, 255, 0.6)',
            // 'rgba(255, 159, 64, 0.6)',
            // 'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        },
        {
          label:'Male',
          data:[
            8377617,
            6241224,
            4213985,
            2787292,
            2345544,
            2244340
          ],
          //backgroundColor:'green',
          backgroundColor:[
            'rgba(75, 192, 192, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(75, 192, 192, 0.6)'
            // 'rgba(54, 162, 235, 0.6)',
            // 'rgba(255, 206, 86, 0.6)',
            // 'rgba(75, 192, 192, 0.6)',
            // 'rgba(153, 102, 255, 0.6)',
            // 'rgba(255, 159, 64, 0.6)',
            // 'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'  
        },
        {
          label:'Female',
          data:[
            8375617,
            6237224,
            4211985,
            2783292,
            2335544,
            2242340
          ],
          //backgroundColor:'green',
          backgroundColor:[
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            // 'rgba(54, 162, 235, 0.6)',
            // 'rgba(255, 206, 86, 0.6)',
            // 'rgba(75, 192, 192, 0.6)',
            // 'rgba(153, 102, 255, 0.6)',
            // 'rgba(255, 159, 64, 0.6)',
            // 'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'  
        }]
      },
      options:{
        title:{
          display:true,
          text:'Largest Cities In India',
          fontSize:25
        },
        legend:{
          display:true,
          position:'right',
          labels:{
            fontColor:'#000'
          }
        },
        layout:{
          padding:{
            left:50,
            right:0,
            bottom:0,
            top:0
          }
        },
        tooltips:{
          enabled:true
        }
      }
    });
    // this.myChart = new Chart (ctx, {
    //   type: 'bar',
    //   data: [{x:'2016-12-25', y:20}, {x:'2016-12-26', y:10}],
    //   options: {
    //     scales: {
    //       xAxes: [{
    //         stacked: true
    //     }],
    //     yAxes: [{
    //         stacked: true
    //     }]
    //     }
    //   }
    // });
    // console.log(this.myChart);
  //   this._weather.dailyForecast()
  //   .subscribe(res => {
  //     console.log(res);

  //     let temp_max = res['list'].map(res => res.main.temp_max)
  //     let temp_min = res['list'].map(res => res.main.temp_min)
  //     let alldates = res['list'].map(res => res.dt)

  //     let weatherDates = []
  //     alldates.forEach((res) => {
  //       let jsdate = new Date(res * 1000)
  //       weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric'}))
  //     })

  //     this.chart = new Chart('canvas', {
  //       type: 'line',
  //       data: {
  //         labels: weatherDates,
  //         datasets: [
  //           {
  //             data: temp_max,
  //             borderColor: '#3cba9f',
  //             fill: false
  //           },
  //           {
  //             data: temp_min,
  //             borderColor: '#ffcc00',
  //             fill: false
  //           },
  //         ]
  //       },
  //       options: {
  //         legend: {
  //           display: false
  //         },
  //         scales: {
  //           xAxes: [{
  //             display: true
  //           }],
  //           yAxes: [{
  //             display: true
  //           }]
  //         }
  //       }
  //     });
  //   });
   }



}
