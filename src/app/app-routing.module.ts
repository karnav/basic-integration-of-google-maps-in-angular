import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GooglemapComponent } from './googlemap/googlemap.component';
import { ChartsComponent } from './charts/charts.component';
import { ExcelComponent } from './excel/excel.component';

const routes: Routes = [
  {path: 'googlemap', component: GooglemapComponent},
  {path: 'charts', component: ChartsComponent },
  {path: 'excel', component: ExcelComponent},
  {path: '', redirectTo: '/googlemap', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
